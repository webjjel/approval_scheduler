let target = ['sac', 'chakra', 'mirage', 'nac', 'vpn'],
    targetCount = target.length,
    diffDay = 5;

// event binding
document.addEventListener('DOMContentLoaded', function() {
    let btnSave = document.querySelector('#btnSave');
    btnSave.addEventListener('click', function() {
        saveData();
    })
});

function saveData()
{
    let data = {};

    for (let i = 0; i < targetCount; i++) {
        data[target[i]] = document.querySelector('#' + target[i]).value;
    }

    chrome.storage.sync.set(data, function() {
        sendMessage('setBadge', function() {
            alert('저장 완료');
        });
    });
}

function getAndSetData()
{
    let data = [];

    chrome.storage.sync.get(target, function(googleData) {
        for (let i = 0; i < targetCount; i++) {
            data[target[i]] = googleData[target[i]] || null;

            if (data[target[i]]) {
                document.querySelector('#' + target[i]).value = data[target[i]];
            }
        }                                                
    });
}

function sendMessage(message, callback)
{
    chrome.runtime.sendMessage(message, callback);
}

window.onload = getAndSetData;